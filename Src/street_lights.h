#include <iostream>

using namespace std;

inline int solStreetLights(const bool* lightMap, int len){
    if (len == 1) return !lightMap[0];
    if (len == 2) return !(lightMap[0] || lightMap[1]);

    auto processSegment = [lightMap](int current, int& last, int& result){
        int segmentLen = ((current - 1) - (last + 1)) - 1;

        if (segmentLen > 0) {
            // Get number of chunks:
            // full size = 3
            // remainder size < 3, it's located in the end
            result += (segmentLen / 3) + ((segmentLen % 3) > 0);
        }

        last = current;
    };

    int result = 0;
    int lastLightIdx;

    // Check for light at the beginning:
    if (!lightMap[0] && !lightMap[1]){
        lastLightIdx = 1;
        result++;

    } else {
        lastLightIdx = lightMap[1] ? 1 : 0;
    }

    // Check rest of the road:
    for (int c = 2; c < len; ++c){
        // Order IMPORTANT:
        if (lightMap[c]){
            processSegment(c, lastLightIdx, result);

        } else if (c == len - 1){
            // Move current out of the road bounds
            processSegment(c + 2, lastLightIdx, result);
        }
    }

    return result;
}

// FIXME: Ugly, Only for testing purposes
int solSlowStreetLights(const bool *lightMap, int len){
    bool *canvas = new bool[len];
    int result = 0;

    // Prepare canvas:
    for (int c = 0; c < len; ++c){
        if (lightMap[c]){
            canvas[c] = true;
            if (c > 0) canvas[c - 1] = true;
            if (c < len - 1) canvas[c + 1] = true;
        }
    }

    // Iterate over canvas:
    for (int c = 0; c < len; ++c){
        if (!canvas[c]){
            canvas[c] = true;

            if (c < len - 1 && !canvas[c + 1]){
                canvas[c + 1] = true;
                if (c < len - 2) canvas[c + 2] = true;
            }

            result++;
        }
    }

    delete[] canvas;
    return result;
}

int _street_lights_dbg(){
    const int len = 10;

    //const bool fixed[] = {1, 1, 0, 0, 1, 0, 1, 1, 0, 1};
    //cout << solStreetLights(&fixed[0], sizeof(fixed)); return 1;

    srand(time(NULL));

    for (;;) {
        bool* lightMap = new bool[len];
        for (int i = 0; i < len; ++i) lightMap[i] = rand() % 2;

        // Find number of lights that needed to fully light the road
        int normal = solStreetLights(&lightMap[0], len);
        int canvas = solSlowStreetLights(&lightMap[0], len);

        if (normal != canvas) {
            cout << "Diff: " << normal << ' ' << canvas << "\n";

            for (int i = 0; i < len; ++i) cout << lightMap[i] << " ";
            cout << "\n";
        }

        delete[] lightMap; // free mem
    }
}