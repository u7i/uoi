#include <iostream>
#include <algorithm>
#include <vector>
#include <chrono>

using namespace std;
using namespace std::chrono;

using TimePoint = std::chrono::_V2::system_clock::time_point;

inline int solBoxesDbg(vector<int> &boxes, int capacity, vector<TimePoint> &timePoints){
    int boxesNum = boxes.size();
    int result = 0;

    // Sort array:
    sort(boxes.begin(), boxes.end());
    timePoints.push_back(high_resolution_clock::now());

    // Check for impossible case:
    if (boxes[boxesNum - 1] > capacity){
        cout << "Impossible\n";
        return 0;
    }

    for (int i = 0; i < boxesNum; ++i){
        for (int c = boxesNum - 1; c > i; --c){
            boxesNum--; // shrink amount of free boxes

            if ((boxes[i] + boxes[c]) <= capacity){
                break;
            }

            result++; // this box can only be moved without pair
        }

        result++;
    }

    return result;
}

int _boxes_dbg() {
    const int boxesNum = 4, capacity = 2;

    // Generate test-set:
    vector<int> boxes;
    boxes.resize(boxesNum);

    // Fill array with the data:
    srand(time(NULL));
    for (int i = 0; i < boxesNum; ++i) boxes[i] = 1 + rand() % (capacity);

    std::vector<TimePoint> timePoints;

    // Exec test:
    auto execTime = high_resolution_clock::now();

    int result = solBoxesDbg(boxes, capacity, timePoints);

    timePoints.push_back(high_resolution_clock::now());

    // Calculate time points:
    cout << "Execution finished:\n";
    cout << "Result: " << result << "\n";

    cout << "Time points: \n";
    for (const auto &tp : timePoints){
        cout << duration_cast<milliseconds>(tp - execTime).count() << "(ms)\n";
    }

    return 0;
}