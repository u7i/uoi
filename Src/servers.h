#include <iostream>
#include <map>
#include <algorithm>
#include <vector>

using namespace std;

using IPAddr = std::array<unsigned char, 4>;

struct RequestsInfo {
    int overall = 0;
    map<IPAddr, int> detailed;
};

inline void printIPAddress(const IPAddr &address, char endWith) {
    for (int c = 0; c < 3; ++c) cout << (int)address[c] << '.';
    cout << (int)address[3] << endWith;
}

inline void readIPAddress(IPAddr &address){
    int tmp;
    for (int c = 0; c < 4; ++c){
        cin >> tmp;
        address[c] = (unsigned char)tmp;
    }
}

int main(){
    map<IPAddr, RequestsInfo> log;

    int n;
    cin >> n;

    // Parse input:
    IPAddr src, dest;
    for (int c = 0; c < n; ++c){
        readIPAddress(src);
        readIPAddress(dest);

        RequestsInfo &info = log[src];

        info.overall++;
        info.detailed[dest]++;
    }

    // Find most active IP-address:
    const IPAddr *addr = nullptr;
    const RequestsInfo *info = nullptr;

    for (const auto &pair : log){
        if (!info || pair.second.overall > info->overall){
            addr = &pair.first;
            info = &pair.second;
        }
    }

    // Prepare data for printing:
    vector <pair<IPAddr, int>> receivers(info->detailed.size());

    int c = 0;
    for (const auto &pair : info->detailed){
        receivers[c] = pair;
        ++c;
    }

    // Sort with custom comparator:
    sort(receivers.begin(), receivers.end(),
         [](const pair<IPAddr, int> &f, pair<IPAddr, int> &s){

             if (f.second < s.second) return true;

             // Cmp by IP addresses ( in increasing order )
             if (f.second == s.second) {
                 for (int c = 0; c < 4; ++c) {
                     if (f.first[c] > s.first[c]) return true;
                 }
             }

             return false;
         });

    // Print data:
    printIPAddress(*addr, '\n');
    for (int c = receivers.size() - 1; c >= 0; --c){
        std::cout << receivers[c].second << " - ";
        printIPAddress(receivers[c].first, '\n');
    }
}