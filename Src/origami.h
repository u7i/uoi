#include <iostream>

using uint = unsigned int;

template<typename T>
T fit2Bounds(const T &v, const T &min, const T &max) {
    if (v < min) return min;
    if (v > max) return max;

    return v;
}

struct Friend {
    const uint productionTime = 0;
    const uint quantity = 0;

    const uint timeout = 0;

    // Calculated-props:
    const uint workTime = 0;
    const uint cycleLen = 0;

    // C-tors:
    Friend() = default;
    Friend(uint productionTime, uint quantity, uint timeout)
    // Set props:
            : productionTime(productionTime), quantity(quantity), timeout(timeout),
              workTime(productionTime * quantity),
              cycleLen(productionTime * quantity + timeout) {

    }

    Friend(const Friend &other) = default;

    // Public API:
    inline uint outputInTime(uint time) const {
        return (time / this->cycleLen) * this->quantity +
               (fit2Bounds<uint>(time % this->cycleLen, 0, this->workTime) / this->productionTime);
    }

    // Operators:
    Friend &operator=(const Friend &other) {
        if (&other == this) return *this;

        *(const_cast<uint *>(&this->productionTime)) = other.productionTime;
        *(const_cast<uint *>(&this->quantity)) = other.quantity;
        *(const_cast<uint *>(&this->timeout)) = other.timeout;

        *(const_cast<uint *>(&this->workTime)) = other.workTime;
        *(const_cast<uint *>(&this->cycleLen)) = other.cycleLen;

        return *this;
    }
};

inline uint solOrigami(uint toysNum, const Friend *friends, uint friendsNum) {
    if (friendsNum == 0) return 0;

    // Do calculations:
    int time = 1;

    [&] {
        for (;; time++) {
            int curToysNum = 0;

            for (int i = 0; i < friendsNum; ++i) {
                curToysNum += friends[i].outputInTime(time);

                if (curToysNum >= toysNum) {
                    return;
                }
            }
        }
    }();

    return time;
}

int _origami() {
    uint friendsNum, toysNum;
    Friend *friends;

    // Read input
    std::cin >> toysNum >> friendsNum;
    friends = new Friend[friendsNum];

    uint productionTime, quantity, timeout;
    for (uint c = 0; c < friendsNum; ++c) {
        std::cin >> productionTime >> quantity >> timeout;
        friends[c] = Friend(productionTime, quantity, timeout);
    }

    // Solve the problem:
    std::cout << solOrigami(toysNum, friends, friendsNum) << '\n';

    delete[] friends;
    return 0;
}