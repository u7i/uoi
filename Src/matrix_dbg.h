#include <iostream>
#include <vector>
#include <chrono>

using namespace std;
using namespace std::chrono;

using TimePoint = std::chrono::_V2::system_clock::time_point;

struct Vec2i {
    int x, y;
    Vec2i(int x, int y) : x(x), y(y) {}
};

inline int dist(int n, const Vec2i &matrix) {
    //n -= 1;

    /*Vec2i hPos = {n % matrix.x,  n / matrix.x};
    Vec2i vPos = {n / matrix.y, n % matrix.y};

    return abs((hPos.y - vPos.y)*matrix.x + (hPos.x - vPos.x)); */

    // (w*h - 1)*(n/h) + n - n*w
    return (matrix.x * matrix.y - 1) * (n / matrix.y) + n - n * matrix.x;
}

inline int solMatrixDbg(const Vec2i &matrix) {
    int result = 2;

    if (matrix.x == 1) return matrix.y;
    if (matrix.y == 1) return matrix.x;

    // (c % matrix.x) == c / matrix.y && (c % matrix.y) == c / matrix.x
    for (int c = 1; c < matrix.x * matrix.y - 1; ++c) {
        if ((c % matrix.x) == c / matrix.y && (c % matrix.y) == c / matrix.x) {
            //std::cout << c + 1 << ' ' << c % matrix.x << ' ' << c / matrix.y << " - slow sol\n";
            result++;
        }
    }

    return result;
}

inline int solMatrix(const Vec2i &matrix) {
    int result = 2;

    // WARN: Those two if-s are REALLY important
    // We make an assumption that every column has only 1 valid intersection
    // But it won't work if x == 1 or y == 1
    if (matrix.x == 1) return matrix.y;
    if (matrix.y == 1) return matrix.x;

    //if (matrix.x == matrix.y) return matrix.x;

    // Some math-magic
    uint64_t last = (uint64_t) matrix.x * (uint64_t) matrix.y - 1;
    for (uint x = 1; x < matrix.x - 1; ++x) {
        uint64_t beginN = ((uint64_t) x) * matrix.y;

        // (matrix.x*matrix.y - 1)(beginN // matrix.y) = n(matrix.x - 1)
        uint64_t possibleN = (last * x) / (matrix.x - 1);

        //std::cout << possibleN << "\n";

        if ((possibleN % matrix.x) == possibleN / matrix.y &&
            (possibleN % matrix.y) == possibleN / matrix.x) {

            //std::cout << x << ' ' << possibleN + 1 << '\n';
            ++result;
        }
    }

    return result;
}

int _matrix_dbg() {
    const int w = 1, h = 1;

    std::vector<TimePoint> timePoints;
    int result[2];

    // Exec test:
    auto execTime = high_resolution_clock::now();

    for (int x = 1; x < 100; ++x) {
        for (int y = 1; y < 100; ++y) {
            result[0] = solMatrixDbg({x, y});
            result[1] = solMatrix({x, y});

            if (result[0] != result[1]) {
                std::cout << "Failed: " << x << ' ' << y << ' '
                          << result[0] << ' ' << result[1] << '\n';
            } else {
                //std::cout << "Accepted: " << x << ' ' << y << ' '
                //          << result[0] << ' ' << result[1] << '\n';
            }
        }
    }

    //result[0] = solMatrix({3, 3});

    timePoints.push_back(high_resolution_clock::now());

    // Calculate time points:
    cout << "Execution finished:\n";
    cout << "Result: " << result[0] << ' ' << result[1] << "\n";

    cout << "Time points: \n";
    for (const auto &tp: timePoints) {
        cout << duration_cast<milliseconds>(tp - execTime).count() << "(ms)\n";
    }

    return 0;
}