#include <iostream>
#include <string>

using uint = unsigned int;

struct Team {
    std::string name {};

    int solvedTasks = -1;
    uint penalty = 0;
};

int _who_is_the_winner(){
    uint testsNum;
    std::cin >> testsNum;

    Team result[testsNum];

    uint teamsNum;
    Team tmp;
    for (uint c = 0; c < testsNum; ++c){
        Team &best = result[c];

        std::cin >> teamsNum;
        for (uint i = 0; i < teamsNum; ++i) {
            std::cin >> tmp.name >> tmp.solvedTasks >> tmp.penalty;

            if (tmp.solvedTasks > best.solvedTasks
                || (tmp.solvedTasks == best.solvedTasks && tmp.penalty < best.penalty)) {

                best = tmp;
            }
        }
    }

    for (const Team &best : result){
        std::cout << best.name << "\n";
    }

    return 0;
}