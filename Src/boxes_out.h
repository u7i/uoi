#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

inline int solBoxes(vector<int> &boxes, int capacity){
    int boxesNum = boxes.size();
    int result = 0;

    // Sort array:
    sort(boxes.begin(), boxes.end());

    // Check for impossible case:
    if (boxes[boxesNum - 1] > capacity){
        return 0;
    }

    for (int i = 0; i < boxesNum; ++i){
        for (int c = boxesNum - 1; c > i; --c){
            boxesNum--; // shrink amount of free boxes

            if ((boxes[i] + boxes[c]) <= capacity){
                break;
            }

            result++; // this box can be moved only without pair
        }

        result++;
    }

    return result;
}

int _boxes(){
    int boxesNum, capacity;
    vector<int> boxes;

    cin >> boxesNum >> capacity;

    // Allocate array:
    boxes.resize(boxesNum);

    // Fill array with the data:
    for (int i = 0; i < boxesNum; ++i) cin >> boxes[i];

    // Calculate result:
    int result = solBoxes(boxes, capacity);

    if (result == 0) cout << "Impossible\n";
    else cout << result << "\n";

    return 0;
}