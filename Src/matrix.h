#include <iostream>

using uint = unsigned int;

struct Vec2i {
    int x, y;

    Vec2i(int x, int y) : x(x), y(y) {}
};

inline int solMatrix(const Vec2i &matrix) {
    int result = 2;

    // WARN: Those two if-s are REALLY important
    // We make an assumption that every column has only 1 valid intersection
    // But it won't work if x == 1 or y == 1
    if (matrix.x == 1) return matrix.y;
    if (matrix.y == 1) return matrix.x;

    if (matrix.x == matrix.y) return matrix.x;

    // Some math-magic
    uint64_t last = (uint64_t) matrix.x * (uint64_t) matrix.y - 1;
    for (uint x = 1; x < matrix.x - 1; ++x) {
        uint64_t beginN = ((uint64_t) x) * matrix.y;

        // (matrix.x*matrix.y - 1)(beginN // matrix.y) = n(matrix.x - 1)
        uint64_t possibleN = (last * x) / (matrix.x - 1);

        if ((possibleN % matrix.x) == possibleN / matrix.y &&
            (possibleN % matrix.y) == possibleN / matrix.x) {

            ++result;
        }
    }

    return result;
}

int main() {
    Vec2i matrix{0, 0};
    std::cin >> matrix.x >> matrix.y;

    std::cout << solMatrix(matrix);
    return 0;
}