#include <iostream>
#include <string>

using uint = unsigned int;

enum TaskState {
    TS_None = 0,
    TS_Solved = 1,
    TS_Wrong = 2
};

struct Task {
    TaskState state = TS_None;

    uint triesNum = 0;
    uint lastTryTime = 0;
};

int _icpc(){
    Task tasks[26]; // for each alphabetical letter

    uint num;
    std::cin >> num;

    unsigned char taskID;
    std::string taskState;
    uint taskTime;

    for (int c = 0; c < num; ++c) {
        std::cin >> taskTime >> taskID >> taskState;

        if (taskID < 65 || taskID > 90) return 1; // Out-of-Bounds
        taskID -= 65;

        // Parse state
        if (taskState == "right") tasks[taskID].state = TS_Solved;
        else if (taskState == "wrong") tasks[taskID].state = TS_Wrong;
        else tasks[taskID].state = TS_None;

        tasks[taskID].triesNum++;
        tasks[taskID].lastTryTime = taskTime;
    }

    uint penalty = 0, solvedNum = 0;
    for (const Task &task : tasks){
        if (task.state == TS_Solved){
            penalty += task.lastTryTime + 20*(task.triesNum - 1);
            solvedNum++;
        }
    }

    std::cout << solvedNum << ' ' << penalty << "\n";
    return 0;
}