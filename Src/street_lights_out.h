#include <iostream>

using namespace std;

inline int solStreetLights(const bool* lightMap, int len){
    if (len == 1) return !lightMap[0];
    if (len == 2) return !(lightMap[0] || lightMap[1]);

    auto processSegment = [lightMap](int current, int& last, int& result){
        int segmentLen = ((current - 1) - (last + 1)) - 1;

        if (segmentLen > 0) {
            // Get number of chunks:
            // full size = 3
            // remainder size < 3, it's located in the end
            result += (segmentLen / 3) + ((segmentLen % 3) > 0);
        }

        last = current;
    };

    int result = 0;
    int lastLightIdx;

    // Check for light at the beginning:
    if (!lightMap[0] && !lightMap[1]){
        lastLightIdx = 1;
        result++;

    } else {
        lastLightIdx = lightMap[1] ? 1 : 0;
    }

    // Check rest of the road:
    for (int c = 2; c < len; ++c){
        // Order IMPORTANT:
        if (lightMap[c]){
            processSegment(c, lastLightIdx, result);

        } else if (c == len - 1){
            processSegment(c + 2, lastLightIdx, result);
        }
    }

    return result;
}

int _street_lights(){
    int testsNum;
    int* results;

    cin >> testsNum;
    results = new int[testsNum];

    // Do tests:
    for (int c = 0; c < testsNum; ++c){
        bool* lightMap;
        int len;

        cin >> len;
        lightMap = new bool[len];

        // Gather data from the input:
        char tmp;
        for (int i = 0; i < len; ++i){
            cin >> tmp;
            switch (tmp){
                case '.':
                    lightMap[i] = false;
                    break;

                case '*':
                    lightMap[i] = true;
                    break;

                default: break;
            }
        }

        // Find number of lights that needed to fully light the road
        results[c] = solStreetLights(&lightMap[0], len);

        delete[] lightMap; // free mem
    }

    // Print results:
    for (int c = 0; c < testsNum; c++) cout << results[c] << "\n";
    delete[] results;
}