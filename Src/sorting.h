#include <iostream>

int _sorting() {
    int *data;
    int n, swaps = 0;

    std::cin >> n;
    data = new int[n];

    for (int i = 0; i < n; ++i) std::cin >> data[i];

    // Sorting impl (n^2):
    int maxIdx, tmp;
    for (; n > 0; --n) {
        maxIdx = 0;
        for (int i = 0; i < n; ++i) {
            if (data[maxIdx] < data[i]) maxIdx = i;
        }

        if (maxIdx == (n - 1)) continue;

        // Swap impl:
        tmp = data[n - 1];
        data[n - 1] = data[maxIdx];
        data[maxIdx] = tmp;

        swaps++;
    }

    std::cout << swaps << '\n';

    delete[] data;
    return 0;
}